# Water Boa Python

An attempt to create an A******a-like Python basis,
entirely based on conda-forge and other free sources.

## Strategy

The package list of an A******a release is inspected and
transferred to an `environment.yml` file.  Most version
specifications are removed, except for few key packages
such as Python itself, NumPy, SciPy, etc. which are kept.
Packages that are obviously dependencies not listed to
make the dependency resolution work more flexibly.

Packages are downloaded entirely from conda-forge and not from the
default and anaconda channels which would require licensing.

## Files

The following files are located in a subdirectory labeled
by a release tag (e.g. 2024.06).

* `config.sh`: Configuration settings.
* `environment.yml`: Specification of the base environment of the Water Boa installation.
* `install_waterboa.sh`: Installation script, can be run either in online mode or in offline mode, cf. `config.sh`
