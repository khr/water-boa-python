# configuration variables, used by <install_waterboa.sh>

URL="https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-Linux-x86_64.sh"
INSTALLER=`basename $URL`

PYTHON_VERSION="3.12.4"
INSTALL_OFFLINE=${INSTALL_OFFLINE:-0}

CONDA_PKGS_DIRS_BASE=`pwd`/pkgs_base
CONDA_PKGS_DIRS_EXT=`pwd`/pkgs_ext
mkdir -p $CONDA_PKGS_DIRS_BASE $CONDA_PKGS_DIRS_EXT

# for an actual installation, set PREFIX to a reasonable path
PREFIX=${PREFIX:-/scratch/tmp/waterboa}
mkdir -p $PREFIX
PREFIX=`readlink -f $PREFIX`
