#!/bin/bash
#set -x
set -e

source config.sh

echo ""
if [ x"$INSTALL_OFFLINE" == x"0" ]
then
    echo "### ONLINE MODE, DOWNLOADING PACKAGES ###"
    if [ ! -f $INSTALLER ]; then
        wget $URL
    fi
else
    echo "### OFFLINE MODE, EXPECTING PACKAGES LOCALLY ###"
fi
echo ""

ls -l $INSTALLER && echo "OK!"

bash $INSTALLER -f -b -p $PREFIX


source $PREFIX/bin/activate

if [ x"$INSTALL_OFFLINE" == x"0" ]
then
    export CONDA_PKGS_DIRS=$CONDA_PKGS_DIRS_BASE
    conda install --yes python="${PYTHON_VERSION}"

    export CONDA_PKGS_DIRS=$CONDA_PKGS_DIRS_EXT
    conda env update --file environment.yml --prune --name base
else
    export CONDA_PKGS_DIRS=$CONDA_PKGS_DIRS_BASE
    conda install --offline --yes python="${PYTHON_VERSION}"

    export CONDA_PKGS_DIRS=$CONDA_PKGS_DIRS_EXT
    ls ${CONDA_PKGS_DIRS}/*.conda ${CONDA_PKGS_DIRS}/*.bz2 | xargs -i conda install --yes {}
fi

conda list

source $PREFIX/bin/deactivate

ls -l $PREFIX
